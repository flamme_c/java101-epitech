package net.epitech.java.td01.service.impl;

import java.util.Collection;
import java.util.HashSet;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import net.epitech.java.td01.model.Course;
import net.epitech.java.td01.model.Teacher;
import net.epitech.java.td01.service.contract.Service;

//TODO:this class should be unit tested
@org.springframework.stereotype.Service
public class ServiceImpl implements Service
{
	public ServiceImpl()
	{
		this.idCourse = 0;
		this.idTeacher = 0;
		this.courses = new HashSet<Course>();
		this.teachers = new HashSet<Teacher>();
		try {
			this.addTeacher("Norris", "norris_c@epitech.eu");
			this.addTeacher("Noel", "noel_p@epitech.eu");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Collection<Course> getCourses()
	{
		return this.courses;
	}

	public Collection<Teacher> getTeachers()
	{
		return this.teachers;
	}

	public void addCourse(String name, DateTime date, Duration duration, Teacher t) throws Exception
	{
		boolean exist = false;

		for (Course c : this.courses)
		{
			if (false) //TODO: chevauchement
				exist = true;
		}
		if (!exist)
		{
			Course c = new Course();
			c.setId(this.idCourse++);
			c.setName(name);
			c.setDate(date);
			c.setDuration(duration);
			c.setTeacher(t);
			this.courses.add(c);			
		}
		else
			throw new Exception("Course already exist");
	}

	public void deleteCourse(Integer id) throws Exception
	{
		boolean exist = false;
		Collection<Course> coursetodelete = new HashSet<Course>();

		for (Course c : this.courses)
		{
			if (c.getId().equals(id))
			{
				coursetodelete.add(c);
				exist = true;
			}
		}
		this.courses.removeAll(coursetodelete);
		if (!exist)
			throw new Exception("Course does not exist");
	}

	public void addTeacher(String name, String mail) throws Exception
	{
		boolean exist = false;

		for (Teacher t : this.teachers)
		{
			if ((t.getName() + t.getMail()) == (name + mail))
				exist = true;
		}
		if (!exist)
		{
			Teacher t = new Teacher();
			t.setId(this.idTeacher++);
			t.setName(name);
			t.setMail(mail);
			this.teachers.add(t);			
		}
		else
			throw new Exception("Teacher already exist");
	}

	public void deleteTeacher(Integer id) throws Exception
	{
		boolean exist = false;
		Collection<Teacher> teacherstodelete = new HashSet<Teacher>();

		for (Teacher t : this.teachers)
		{
			if (t.getId().equals(id))
			{
				teacherstodelete.add(t);
				exist = true;
			}
		}
		this.teachers.removeAll(teacherstodelete);
		if (!exist)
			throw new Exception("Teacher does not exist");			
	}

	public Collection<Pair<DateTime, DateTime>> getAvaibleTimeSlot(Duration d)
	{
		Collection<Pair<DateTime, DateTime>> avaibleTimeSlot = new HashSet<Pair<DateTime, DateTime>>();
		return avaibleTimeSlot;
	}
	
	private Collection<Course>	courses;
	private Collection<Teacher>	teachers;
	private Integer				idTeacher;
	private	Integer				idCourse;

}
