package net.epitech.java.td01.td0ioc.model;

import net.epitech.java.td01.td0ioc.model.contract.Teachable;

public abstract class AbstractTeachable implements Teachable {

	@Override
	public String toString() {
		return String.format(" \"%s\" in %s by %s (%s)", this
				.getCourseMaterial(), this.getCourse().name(), this
				.getTeacher().getName(), this.getTeacher().getSex().name());
	}

}
