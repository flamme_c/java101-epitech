package net.epitech.java.td01.service.contract;

import java.util.Collection;

import net.epitech.java.td01.model.Course;
import net.epitech.java.td01.model.Teacher;

//TODO: interface must be throughly documented
public interface Service
{
	public Collection<Course>	getCourses();
	public Collection<Teacher>	getTeachers();
	public void					addCourse(Course c);

	public void					deleteCourse(Course c);
	//TODO: this method should return void and handle errors using exception
	public boolean				addTeacher(String name, String mail, Integer id);
	//TODO: this method should return void and handle errors using exception
	public boolean 				deleteTeacher(Integer id);
}